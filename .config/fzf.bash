# Auto-completion
# ---------------
[[ $- == *i* ]] && source "/home/martin/.config/fzf/shell/completion.bash" 2> /dev/null

# Key bindings
# ------------
source "$HOME/.config/fzf/shell/key-bindings.bash"

# Configuration
# -------------
export FZF_DEFAULT_OPTS="
--layout=reverse
--info=inline
--height=80%
--multi
--preview-window=:hidden
--preview '([[ -f {} ]] && (bat --style=numbers --color=always {} || cat {})) || ([[ -d {} ]] && (tree -C {} | less)) || echo {} 2> /dev/null | head -200'
--bind '?:toggle-preview'
--bind 'ctrl-a:select-all'
--bind 'ctrl-y:execute-silent(echo {+} | pbcopy)'
--bind 'ctrl-e:execute(echo {+} | xargs -o vim)'
--bind 'ctrl-v:execute(code {+})'
"
#--prompt='∼ ' --pointer='▶' --marker='✓'
#--color='hl:148,hl+:154,pointer:032,marker:010,bg+:237,gutter:008'
# fzf's command
# export FZF_DEFAULT_COMMAND="fd --hidden --follow --exclude '.git' --exclude 'node_modules'"
# export FZF_DEFAULT_COMMAND='rg --files --hidden'
# CTRL-T's command
# export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
# ALT-C's command
# export FZF_ALT_C_COMMAND="$FZF_DEFAULT_COMMAND"
