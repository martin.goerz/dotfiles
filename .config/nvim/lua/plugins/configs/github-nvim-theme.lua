local present, theme = pcall(require, "github-theme")

if not present then
   return
end

theme.setup({
  theme_style = "dark",
})
