local present, icons = pcall(require, "nvim-web-devicons")
if not present then
   return
end


icons.setup {
   override = {
      c = {
         icon = "",
         color = "#146eff",
         name = "c",
      },
      css = {
         icon = "",
         color = "#146eff",
         name = "css",
      },
      deb = {
         icon = "",
         color = "#14ebff",
         name = "deb",
      },
      Dockerfile = {
         icon = "",
         color = "#14ebff",
         name = "Dockerfile",
      },
      html = {
         icon = "",
         color = "#f28500",
         name = "html",
      },
      jpeg = {
         icon = "",
         color = "#9d00f2",
         name = "jpeg",
      },
      jpg = {
         icon = "",
         color = "#9d00f2",
         name = "jpg",
      },
      js = {
         icon = "",
         color = "#f2de00",
         name = "js",
      },
      lock = {
         icon = "",
         color = "#d10f0f",
         name = "lock",
      },
      lua = {
         icon = "",
         color = "#146eff",
         name = "lua",
      },
      mp3 = {
         icon = "",
         color = "#f5f5f5",
         name = "mp3",
      },
      mp4 = {
         icon = "",
         color = "#f5f5f5",
         name = "mp4",
      },
      out = {
         icon = "",
         color = "#f5f5f5",
         name = "out",
      },
      png = {
         icon = "",
         color = "#9d00f2",
         name = "png",
      },
      py = {
         icon = "",
         color = "#14ebff",
         name = "py",
      },
      ["robots.txt"] = {
         icon = "ﮧ",
         color = "#d10f0f",
         name = "robots",
      },
      toml = {
         icon = "",
         color = "#146eff",
         name = "toml",
      },
      ts = {
         icon = "ﯤ",
         color = "#1fa2ff",
         name = "ts",
      },
      ttf = {
         icon = "",
         color = "#f5f5f5",
         name = "TrueTypeFont",
      },
      rb = {
         icon = "",
         color = "#ed32c8",
         name = "rb",
      },
      rpm = {
         icon = "",
         color = "#146eff",
         name = "rpm",
      },
      vue = {
         icon = "﵂",
         color = "#00f068",
         name = "vue",
      },
      woff = {
         icon = "",
         color = "#f5f5f5",
         name = "WebOpenFontFormat",
      },
      woff2 = {
         icon = "",
         color = "#f5f5f5",
         name = "WebOpenFontFormat2",
      },
      xz = {
         icon = "",
         color = "#f2de00",
         name = "xz",
      },
      zip = {
         icon = "",
         color = "#f2de00",
         name = "zip",
      },
   },
}
